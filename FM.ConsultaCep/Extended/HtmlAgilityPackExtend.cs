﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FM.ConsultaCep
{
    public static class HtmlAgilityPackExtend
    {
        public static string Text(this HtmlNode html)
        {
            if (html == null)
                return "";

            return TrataHtml(html.InnerText);
        }

        public static Decimal? Decimal(this HtmlNode html)
        {
            if (!IsNumeric(TrataHtml(html.InnerText)))
                return null;

            return Convert.ToDecimal(TrataHtml(html.InnerText));
        }

        public static DateTime? Date(this HtmlNode html)
        {
            var textoTratado = TrataHtml(html.InnerText);

            if (!IsDate(textoTratado))
                return null;

            return Convert.ToDateTime(textoTratado);
        }

        private static string TrataHtml(string texto)
        {
            return texto.Replace("&nbsp;", "").Trim();
        }

        private static bool IsDate(object objeto)
        {
            if (objeto == null || objeto.ToString() == "")
                return false;

            try
            {
                var data = Convert.ToDateTime(objeto);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static bool IsNumeric(string numero)
        {
            if (string.IsNullOrEmpty(numero))
                return false;

            decimal valor;

            return decimal.TryParse(numero, out valor);

        }
    }
}
