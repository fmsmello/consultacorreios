﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FM.ConsultaCep
{
    public class Endereco
    {
        public String Logradouro { get; set; }
        public String Bairro { get; set; }
        public String Localidade { get; set; }
        public String Uf { get; set; }
        public String Cep { get; set; }
    }
}
