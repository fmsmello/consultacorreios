﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FM.ConsultaCep.Parameters
{
    public class Paginacao
    {
        public int PaginaInicial { get; set; }
        public int PaginaFinal { get; set; }
    }
}
