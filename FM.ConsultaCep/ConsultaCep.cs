﻿using FM.ConsultaCep.Enumerators;
using FM.ConsultaCep.Parameters;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace FM.ConsultaCep
{
    public class ConsultaCepCorreios
    {
        private readonly WebRequestHelper _webRequest;
        private string _url = "http://www.buscacep.correios.com.br/sistemas/buscacep/resultadoBuscaCepEndereco.cfm";


        #region Variáveis privadas
        private string _parametro_busca = string.Empty;
        private TipoCepEnum _tipo_cep;
        private bool _semelhanca;
        #endregion

        #region Propriedades
        public string ParametroBusca { get => _parametro_busca; set => _parametro_busca = value; }
        public TipoCepEnum TipoCep { get => _tipo_cep; set => _tipo_cep = value; }
        public bool Semelhanca { get => _semelhanca; set => _semelhanca = value; }
        #endregion

        #region Construtores
        public ConsultaCepCorreios()
        {
            _webRequest = new WebRequestHelper();
        }
        #endregion

        #region Métodos Privados
        private Endereco ExtrairInformacao(HtmlNode node, int row)
        {
            var endereco = new Endereco()
            {
                Logradouro = node.SelectSingleNode(@"//tr[" + row + "]//td[1]").Text(),
                Bairro = node.SelectSingleNode(@"//tr[" + row.ToString() + "]//td[2]").Text(),
                Localidade = node.SelectSingleNode(@"//tr[" + row + "]//td[3]").Text(),
                Cep = node.SelectSingleNode(@"//tr[" + row + "]//td[4]").Text(),
                Uf = node.SelectSingleNode(@"//tr[" + row + "]//td[3]").Text()
            };

            return endereco;
        }
        private HtmlAgilityPack.HtmlDocument Consultar(int pagInicial, int pagFinal)
        {
            Dictionary<string, object> _colQueryStr = new Dictionary<string, object>();

            System.Net.ServicePointManager.Expect100Continue = false;
            String tpBusca = String.Empty;

            _webRequest.Get(_url);

            switch (_tipo_cep)
            {
                case TipoCepEnum.LocalidadeLogradouro: { tpBusca = "LOG"; break; }
                case TipoCepEnum.CEPPromocional: { tpBusca = "PRO"; break; }
                case TipoCepEnum.CaixaPostalComunitaria: { tpBusca = "CPC"; break; }
                case TipoCepEnum.GrandeUsuario: { tpBusca = "GRU"; break; }
                case TipoCepEnum.UnidadeOperacional: { tpBusca = "UOP"; break; }
                case TipoCepEnum.Todos: { tpBusca = "ALL"; break; }
            }

            _colQueryStr.Add("relaxation", _parametro_busca);
            _colQueryStr.Add("TipoCep", tpBusca);
            _colQueryStr.Add("semelhante", _semelhanca == true ? "S" : "N");
            _colQueryStr.Add("qtdrow", "50");
            _colQueryStr.Add("exata", "S");
            _colQueryStr.Add("pagini", pagInicial.ToString());
            _colQueryStr.Add("pagfim", pagFinal.ToString());

            return _webRequest.Post(_url, _colQueryStr);
        }
        private Paginacao RetornarPaginacao(string texto)
        {
            var reg = new Regex("(\\d+)(\\s+)(a)(\\s+)(\\d+)(\\s+)(de)(\\s+)(\\d+)", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Match mat = reg.Match(texto);

            return new Paginacao() { PaginaInicial = Convert.ToInt32(mat.Groups[1].ToString()), PaginaFinal = Convert.ToInt32(mat.Groups[9].ToString()) };
        }
        #endregion
        public List<Endereco> RetornarEnderecos()
        {
            var enderecos = new List<Endereco>();
            int linhaFinal = 50;
            for (int linhaInicial = 1; linhaInicial < linhaFinal; linhaInicial += 50)
            {
                var html = this.Consultar(linhaInicial, linhaInicial+49);

                var pag = this.RetornarPaginacao(html.DocumentNode.InnerText);

                linhaFinal = pag.PaginaFinal;


                var htmlNodes = html.DocumentNode.SelectNodes("//tr");

                if (htmlNodes != null)
                {
                    var row = 2;
                    foreach (HtmlNode node in htmlNodes)
                    {
                        enderecos.Add(this.ExtrairInformacao(node, row++));
                    }
                }
            }
            return enderecos;
        }
    }
}
