﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FM.ConsultaCep.Enumerators
{
    public enum TipoCepEnum
    {
        LocalidadeLogradouro = 1,
        CEPPromocional = 2, CaixaPostalComunitaria = 3,
        GrandeUsuario = 4, UnidadeOperacional = 5, Todos = 6
    }
}
